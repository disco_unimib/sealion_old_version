from .spotlight import WikidataSpotlightEntityLinker
from .wikifier import WikifierEntityLinker
from .entity_model import Entity
from abc import ABCMeta, abstractmethod
import SPARQLWrapper
#this class permits to add an annotator whe the function is called
#Abstract Class Fusion
'''
:param list_of_annotators: list of annotators used to find Entities in the text
:param document: document to be processed
'''
class Fusion(object):
    # MetaClass attribute for Fusion abstract class
    __metaclass__ = ABCMeta

    def __init__(self,list_of_annotators,document):
        self.list_of_annotators=list_of_annotators #list of annotators (Examples: DBPedia-Spotlight,Wikifier)
        self.entityFinal = set() #final set of uri used to extract final list of Entities
        self.entity_list_final = [] # final list of entities
        self.all_entities = [] #list of all entities (entity.uri,entity)
        self.annotator_list = [] #list of list of Entities linked by annotators

        #call annotator.run for all annotators in list.
        for annotator in self.list_of_annotators:
            document.entities = []
            annotator_entities = set()                                                        ######## Lancio di run di Spotlight e Wikifier
            for entity in annotator.run(document).entities:                                     ######## example of annotator: Spotlight - Wikifier
                #Create Entity_TMP and save it into all_entities list
                entity_operation = Entity_TMP(entity.uri, entity)
                self.all_entities.append(entity_operation)
                #add entity uri to annotator_entities set
                annotator_entities.add(entity.uri)
            #append list of entities to annotator list (create a list of list entities)
            self.annotator_list.append(annotator_entities) #add list of entities to annotator_list

    def get_boundaries(self,entity_1,entity_2):
        entities = []

        entity_set = set()

        for boundary_1 in entity_1.boundaries:
            entity_set.add(boundary_1)

        for boundary_2 in entity_2.boundaries:
            entity_set.add(boundary_2)

        for boundary in entity_set:
            entities.append(boundary)

        return entities

    #create list of entity from a set of uri
    def create_entity_final(self):
        for entity_uri in self.entityFinal:
            added = False
            for entity in self.all_entities:
                if entity_uri == entity.uri:
                    if (added == False):
                        self.entity_list_final.append(entity.entity)
                        added = True
                    else:
                        boundaries = self.get_boundaries(self.entity_list_final[len(self.entity_list_final) - 1],
                                                         entity.entity)
                        self.entity_list_final[len(self.entity_list_final) - 1].boundaries = []
                        self.entity_list_final[len(self.entity_list_final) - 1].boundaries = boundaries

    #Abstract method annotate
    #It must be overwritten by subClasses
    @abstractmethod
    def annotate(self,document):
        pass

#Class Entity_TMP used during the construction of Annotating Entities
class Entity_TMP:

    def __init__(self,uri,entity):
        self.uri=uri
        self.entity=entity

# Class used to union two or more different set of entities linked by different annotators
# E = A U B U C U D .....
class FusionUnion(Fusion):

    def annotate(self,document):
        for annotatorEntities in self.annotator_list:                         ######## annotator_list è un lista con le entità annotate
            self.entityFinal = self.entityFinal.union(annotatorEntities)      ########entityFinal è un set e union è un suo metodo
        self.create_entity_final()

        intersection_entities = self.get_intersection()
        for entity in self.entity_list_final:
            if entity.uri in intersection_entities:
                entity.in_all = True

        self.add_text_and_image()

        document.entities = []

        for entity in self.entity_list_final:
            if (len(entity.boundaries) != 0):
                document.entities.append(entity)
        for ent in document.entities:
            print(f"fusion :{ent.uri}")
        return document
    
    #get intersection to find entities annotated by Wikifier and Spotlight
    def get_intersection(self):
        entity_intersection = set()
        entity_intersection = self.annotator_list[0]
        for annotatorEntities in self.annotator_list:
            entity_intersection = entity_intersection.intersection(annotatorEntities)
        return entity_intersection
    
    #get image and text from DBpedia about every entity to create popover in the User Interface
    def add_text_and_image(self):
        for entity in self.entity_list_final:
            stringQuery = "SELECT ?text ?img where { <" + entity.uri + "> dbo:abstract ?text . <" + entity.uri + "> dbo:thumbnail ?img . FILTER langMatches( lang(?text), 'EN' ) }"
            sparql = SPARQLWrapper.SPARQLWrapper("http://dbpedia.org/sparql")
            sparql.setQuery(stringQuery)
            sparql.setReturnFormat(SPARQLWrapper.JSON)
            results = sparql.query().convert()
            result = results['results']['bindings']
            for img_and_text in result:
                entity.text = img_and_text['text']['value']
                entity.img = img_and_text['img']['value']

# Class used to intersect two or more different set of entities linked by different annotators
# E = A INTERSECT B INTERSECT C INTERSECT D .....
class FusionIntersection(Fusion):

    def annotate(self,document):
        # to execute Intersection it's necessary to assign
        # the first set of Entity to entityFinal
        self.entityFinal = self.annotator_list[0]
        for annotatorEntities in self.annotator_list:
            self.entityFinal = self.entityFinal.union(annotatorEntities)   
            # entityFinal is a set
            # the method was intersection, union seems to perform better
        self.create_entity_final()
        for entity in self.entity_list_final:
            entity.in_all = True
        document.entities = self.entity_list_final
        return document

