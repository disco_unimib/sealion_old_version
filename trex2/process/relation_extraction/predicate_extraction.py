from trex2.process.relation_extraction.JSON_operations import *
from trex2.process.relation_extraction.relation_model import Entity_Sentence, Ontology, Couple, Sentence_model, Triple, Predicate
from trex2.process.relation_extraction.abstat import abstat_call, call_related_words_api
from trex2.process.relation_extraction.query_Sparql_DBpedia import query_sparql_to_dbpedia, parse_dbpedia_response, get_equivalent_class
from trex2.process.relation_extraction.predicate_choice import wordnet_synonyms, wordnet_similarity, similarity_between_verb_predicate, find_terms_verb_predicate

import json
import re

from nltk.tokenize import WordPunctTokenizer  ################## [Matt]


def predicate_annotation(document, dates, regex):
    sentences = []
    #### [Matt]
    #document = modify_entity_with_annotator(document)
    ####

    # every cycle analizes a sentence
    for sid, (start, end) in enumerate(document.sentences_boundaries):
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
        # sentence analyzed
        sentence = document.text[start:end]
        # print sentence
        print("sentence= " + document.text[start:end])

        ##### [Matt]
        tokenizer = WordPunctTokenizer()        
        words = list(tokenizer.span_tokenize(sentence))        
        if (len(words)) >= 40:
            continue
        #####


        # list of entities of a sentence
        entity_list_sentence = get_entities(start, end, document)        
        # limit entities
        entity_list_sentence_final = entity_limiter(entity_list_sentence)
        # order entities for boundaries
        entity_list_sentence_final = order_entities(entity_list_sentence_final)           
        # get dates extracted about this sentence    #dates_sentence is a list
        dates_sentence = get_dates(start, end, dates)        
        # get literals extracted about this sentence   #regex_sentence is a list
        regex_sentence = get_regex(start, end, regex)
        # List of Ontology objects with (entity uri, entity name, boundary, list of ontologies of this entity)
        ontology_list = get_ontologies(entity_list_sentence_final)           
        # limit ontologies.....if the list of ontologies for an entity is zero, it will be deleted
        ontology_list_final = ontology_limiter(ontology_list)
        print("ontology_list_final", ontology_list_final)
        # clean otologies = delete ontologies that are more generic
        ontology_list_final = clean_all_ontologies(ontology_list_final)
        print("ontology_list_final clean", ontology_list_final)
        # create couples with dates and regex
        (date_couples, regex_couples) = date_regex_couples(ontology_list_final, dates_sentence, regex_sentence)
        # couple list of entities about a sentence        
        couple_list_of_sentence = couple_calc(ontology_list_final)
        print("couple_list_of_sentence", couple_list_of_sentence)   
        # couple_of _entity_linking + couple_dates + couple_literals
        couple_list_of_sentence_final = couple_list_of_sentence + date_couples + regex_couples  #### type of couple_list_of_sentence = list                               
        # delete multiple couples
        couple_list_of_sentence_final = couple_limiter(couple_list_of_sentence_final)
        # get triples with abstat
        triples = get_triples(couple_list_of_sentence_final, start)
        # check if predicates from abstat (and related words) are in the sentence , start of the sentence in the text        
        (triples, predicates) = check_predicates(triples, sentence, start)
        # takes triples extracted and substitute dbo:Class in the text
        sentence_text = modify_text(document.text, triples, start, end, dates_sentence, regex_sentence)
        # limit triples and predicates
        (predicates, triples) = limit_triples_and_predicates(predicates, triples)
        print("preds", predicates)
        # create Sentence Model
        sentence = Sentence_model(sentence_text, predicates, triples)

        sentences.append(sentence)
    # return list of sentences
    return sentences

##Function for testing
def stampa_triple(triples):
    print("--------------------print Triple---------------------------")
    for tri in triples:
        print("subject"+ tri.subject_uri)
        print("predicate"+ tri.predicate_uri)
        print("object" + tri.object_uri)

def stampa_coppie(coppia):
    print("---------------------print Couple---------------------------")
    for cop in coppia:
        print("subject"+ cop.uri_1)
        print("object" + cop.uri_2)
        print("object" + cop.uri_2)
#####


# get ontologies for entities with sparql query
def get_ontologies(entities):
    ontology_list = []
    for entity in entities:
        result_query = query_type(entity.uri)        
        print("result_query", result_query)
        # Query to obtain Ontologies
        ontologies = parse_query_response(result_query)
        print('ONTOLOGIES', ontologies)
        # Ontology(entity_uri, entity_surfaceform, boundary, list of ontologies of this entity, list of annotator of this entity)
        # create ontology model
        single_ontology = Ontology(entity.uri, entity.surfaceform, entity.boundary, ontologies, entity.annotator)
        ontology_list.append(single_ontology)

    return ontology_list


# get entities that appears in a sentence
def get_entities(start, end, document):
    entity_list_sentence = []    
    for entity in document.entities:
        for boundary in entity.boundaries:
            # if entity is in the current sentence, it is taken
            if (boundary[0] >= start and boundary[1] <= end):
                es = Entity_Sentence(entity.uri, entity.surfaceform, boundary, entity.annotator)
                entity_list_sentence.append(es)

    return entity_list_sentence


# order entities that appear in this sentence
def order_entities(entity_list_final):
    length = len(entity_list_final)
    entities_ordered = []

    while (len(entities_ordered) != length):

        index = entity_list_final[0]
        for entity in entity_list_final:
            if (entity.boundary[0] < index.boundary[0]):
                index = entity

        entities_ordered.append(index)
        entity_list_final.remove(index)

    return entities_ordered


# get dates that appear in this sentence
def get_dates(start, end, dates):
    dates_sentence = []
    for date in dates:
        if (date.boundaries[0] >= start and date.boundaries[1] <= end):
            entity_date = Entity_Sentence(date.uri, date.surfaceform, date.boundaries)
            dates_sentence.append(entity_date)

    return dates_sentence


# get other literals that appear in the sentence
def get_regex(start, end, regex):
    regex_sentence = []
    for reg in regex:
        if (reg.boundary[0] >= start and reg.boundary[1] <= end):
            entity_reg = Entity_Sentence(reg.string_concept, reg.word, reg.boundary )
            regex_sentence.append(entity_reg)

    return regex_sentence


# delete lists without ontologies
def ontology_limiter(ontologies):    
    ontology_list = []
    for ontology in ontologies:
        if (len(ontology.ontology_list) != 0):
            ontology_list.append(ontology)

    return ontology_list


# delete multiple entities (if an entity is already in the list it will not be added)
def entity_limiter(entities):
    entity_list = []

    if (len(entities) != 0):
        for entity in entities:
            if not (is_entity_in(entity, entity_list)):
                entity_list.append(entity)

        return entity_list

    else:
        return entity_list


# return True or False if the entity is in the entity list
def is_entity_in(entity, entities):
    for e in entities:
        if (e.uri == entity.uri):
            return True

    return False


# delete Classes that are more general
# for example if there are dbo:Person and dbo:SoccerPlayer ---> dbo:Person will be deleted
def clean_ontologies(ontology_list):
    ontology_cleaned = []
    ontology_deleted = []
    data = import_json_ontologies()

    # ontology_cleaned.append(ontology_list[0])
    # return ontology_cleaned

    if (len(ontology_list) > 1):
        index = 0
        while index != len(ontology_list):

            first = ontology_list[index].replace('http://dbpedia.org/ontology/', '')

            exist = False
            for ontology in ontology_list:
                o = ontology.replace('http://dbpedia.org/ontology/', '')
                if "" + first + "" in data:
                    if "" + o + "" in data["" + first + ""]:
                        exist = True

            if not exist:
                ontology_cleaned.append('http://dbpedia.org/ontology/' + first)
            else:
                ontology_deleted.append(first)

            index = index + 1

        print("ontologist list"+ str(ontology_list))
        print("ontology cleaned"+ str(ontology_cleaned))

        #Need only the more specific class/delete the equivalent class of the more generic
        if(len(ontology_cleaned) > 1):
            for second in ontology_cleaned:
                for deleted in ontology_deleted:
                    if second == get_equivalent_class(deleted):
                        ontology_cleaned.remove(second)
        return ontology_cleaned

    else:
        return ontology_list


# delete generic ontologies
def clean_all_ontologies(ontologies):
    for ontology in ontologies:
        print("nome dell'entity : " + ontology.surfaceform + " classe collegate: " + str(ontology.ontology_list))
        ontology_list = clean_ontologies(ontology.ontology_list)  ###<<<<----- pulisce la lista di ontologie in Ontology
        ontology.ontology_list = ontology_list

    return ontologies


# create couples formed by: (Entity, Date) and (Entity, Literal)
def date_regex_couples(ontology_list_final, dates_sentence, regex_sentence):
    dates_couple = []
    if (len(dates_sentence) != 0):
        for ontology in ontology_list_final:
            for o in ontology.ontology_list:
                # boundary are none for now
                couple = Couple(o, 'http://www.w3.org/2001/XMLSchema%23date', ontology.boundary, None,
                                ontology.entity_uri, '', ontology.annotator, None)
                dates_couple.append(couple)

    regex_couple = []
    if (len(regex_sentence) != 0):
        for ontology in ontology_list_final:
            for o in ontology.ontology_list:
                # boundary are none for now
                couple = Couple(o, 'http://www.w3.org/2001/XMLSchema%23double', ontology.boundary, None,
                                ontology.entity_uri, '', ontology.annotator, None)
                regex_couple.append(couple)

    return (dates_couple, regex_couple)


# delete double couples
def couple_limiter(couple_list_of_sentence_final):
    couple_list_cleaned = []    

    for couple in couple_list_of_sentence_final:
        found = False
        for couple_2 in couple_list_cleaned:
            if (couple.ontology_1 == couple_2.ontology_1 and couple.ontology_2 == couple_2.ontology_2):
                found = True
        if (found == False):
            couple_list_cleaned.append(couple)

    print('couple_list_cleaned',couple_list_cleaned)
    return couple_list_cleaned


# calculate couples of entities in this sentence
def couple_calc(ontology_list_final):
    couples = []      

    # ontology_list_final contains subj/obj classes and its length must be > 1 which means that we have at least two classes available
    if len(ontology_list_final) > 1:
        index_1 = 0
        while (index_1 < len(ontology_list_final) - 1):
            index_2 = index_1 + 1
            while index_2 < len(ontology_list_final):
                couples = couples + create_couples(index_1, index_2, ontology_list_final)
                index_2 = index_2 + 1
            index_1 = index_1 + 1
        print("couple" + str(couples) )
        return couples
    else:
        print("couple" + str(couples))
        return couples


def create_couples(index_1, index_2, ontology_list_final):
    couples = []
    print("couple index_1",index_1)
    print("couple index_2",index_2)

    for ontology_1 in ontology_list_final[index_1].ontology_list:
        for ontology_2 in ontology_list_final[index_2].ontology_list:
            couple = Couple(ontology_1, ontology_2, ontology_list_final[index_1].boundary,
                            ontology_list_final[index_2].boundary, ontology_list_final[index_1].entity_uri,
                            ontology_list_final[index_2].entity_uri , ontology_list_final[index_1].annotator, ontology_list_final[index_2].annotator)
            couples.append(couple)

    return couples


# calculate triples using ABSTAT and then Related_Words
def get_triples(couple_list_of_sentence, start):
    triples = []

    for couple in couple_list_of_sentence:

        # subject uri
        subject_uri = couple.uri_1
        #print("subject_uri:"+subject_uri)
        # object uri
        object_uri = couple.uri_2
        #print("object_uri:" + object_uri)
        # subject dbo:Class
        subject_ontology = couple.ontology_1
        # object dbo:Class
        object_ontology = couple.ontology_2
        # subject boundary
        subject_boundary = couple.ontology_boundary_1
        # object boundary
        object_boundary = couple.ontology_boundary_2

        # subject_annotator
        subject_annotator=couple.annotator_1
        # object_annotator
        object_annotator=couple.annotator_2

        print("sontology"+ subject_ontology)
        print("oontology" + object_ontology)

        # abstat query (FOR EVERY QUERY IT TAKES SOME TIME)
        predicates = parse_abstat_response(abstat_call(subject_ontology, object_ontology))
        print("predicates con abstat= " + str(predicates))

        # # dbpedia Sparql Query
        # predicates = parse_dbpedia_response(query_sparql_to_dbpedia(subject_ontology,object_ontology))
        # print("predicates con dbpedia= " + str(predicates))

        # create possible triples
        for predicate_uri in predicates:
            triple = Triple(subject_uri, subject_boundary, object_uri, object_boundary, subject_ontology, predicate_uri,
                            object_ontology, subject_annotator, object_annotator)
            
            triples.append(triple)

        print("triples:" + str(triples))

    return triples


# check if the predicate exstracted is in the sentence and if there isn't, check for related words
def check_predicates(triples_candidate, sentence, start):
    english_verbs = import_json_english_verbs()
    triples = []
    predicates = []
    labels = None

    for triple in triples_candidate:
        # get label of the predicate form DBpedia
        label = parse_label_response(query_label(triple.predicate_uri))
        label = re.sub(r'\([^)]*\)|\[[^\]]*\]', "", label)  ###### elimina il contenuto all'interno di parentesi
        if (label != ""):
            matches = check_text(label, sentence, triple, start)
            (triple, predicate) = create_triple_and_predicate(triple, label, matches[0])
            print("TRIPLE",triple)
            triples.append(triple)
            print("PREDICATE",predicate)
            predicates.append(predicate)
            # # try to match label in the sentence
            # # FIRST CHECK -------------------------------------------------------------------
            # matches = check_text(label, sentence, triple, start)
            # for match in matches:
            #     print("MATCH",match)
            #     # if the label match, create Triple and Predicate objects and add them to lists
            #     (triple, predicate) = create_triple_and_predicate(triple, label, match)
            #     print("TRIPLE",triple)
            #     triples.append(triple)
            #     print("PREDICATE",predicate)
            #     predicates.append(predicate)
            # # SECOND CHECK -------------------------------------------------------------------
            # # search if the predicate is in the json of english verbs ad then check if it is in the text
            # if(len(predicates) == 0):
            #     if "" + label.lower() + "" in english_verbs:
            #         verbs = english_verbs["" + label.lower() + ""]
            #         for verb in verbs:
            #             matches = check_text(verb, sentence, triple, start)
            #             for match in matches:
            #                 (triple, predicate) = create_triple_and_predicate(triple, verb, match)
            #                 triples.append(triple)
            #                 predicates.append(predicate)

            # # THIRD CHECK -------------------------------------------------------------------
            # # related word
            # # try to search related words about predicates if abstat's predicates do not match
            # # if (len(predicates) == 0):
            # # # labels = call_related_words_api(label)  ## Avviene un cambio del "oggetto in esamine"
            # # # labels = limit_labels(labels)
            # # # #labels = wordnet_synonyms(label)               #2nd Approach#
            # # # #labels = wordnet_similarity(label, sentence)   #3rd Approach#
            # # # #4th Approach#
            # # # #labels2=[]
            # # # #for lab in labels:
            # # # #    labels2.extend(similarity_between_verb_predicate(lab, sentence))
            # # # #labels = limit_labels(labels2)
            # # # #4th Approch#
            # # # #labels = similarity_between_verb_predicate(label,sentence) #5th Approch#
            # # # #labels = limit_labels(labels)
            # # # for lab in labels:
            # # #     matches = check_text(lab, sentence, triple, start)
            # # #     for match in matches:
            # # #         (triple, predicate) = create_triple_and_predicate(triple, lab, match)
            # # #         triples.append(triple)
            # # #         predicates.append(predicate)
            # # # # FOURTH CHECK -------------------------------------------------------------------
            # # # # search if the predicate is in the json of english verbs ad then check if it is in the text
            # # # # if(len(predicates) == 0):
            # # # for lab in labels:
            # # #     if "" + lab.lower() + "" in english_verbs:
            # # #         verbs = english_verbs["" + lab.lower() + ""]
            # # #         for verb in verbs:
            # # #             matches = check_text(verb, sentence, triple, start)
            # # #             for match in matches:
            # # #                 (triple, predicate) = create_triple_and_predicate(triple, verb, match)
            # # #                 triples.append(triple)
            # # #                 predicates.append(predicate)
            # # # # FIFTH CHECK -------------------------------------------------------------------
            # # # labels = call_related_words_api(label)
            # # # labels = find_terms_verb_predicate(labels,sentence) #5th Approch#
            # # # labels = limit_labels(labels)
            # # # for lab in labels:
            # # #     matches = check_text(lab, sentence, triple, start)
            # # #     for match in matches:
            # # #         (triple, predicate) = create_triple_and_predicate(triple, lab, match)
            # # #         triples.append(triple)
            # # #         predicates.append(predicate)


    return (triples, predicates)


# create triple and predicate objects to save it
def create_triple_and_predicate(triple, lab, match):
    triple.subject = triple.subject_ontology.replace("http://dbpedia.org/ontology/",
                                                     "dbo:")  # parse_label_response(query_label(triple.subject_uri))
    if triple.object_ontology == "http://www.w3.org/2001/XMLSchema%23date":
        triple.object = "xsd:date"
    else:
        if triple.object_ontology == "http://www.w3.org/2001/XMLSchema%23double":
            triple.object = "xsd:double"
        else:
            triple.object = triple.object_ontology.replace("http://dbpedia.org/ontology/",
                                                           "dbo:")  # parse_label_response(query_label(triple.object_uri))

    triple.predicate = "dbo:" + triple.predicate_uri[28:]

    # predicate = Predicate(lab, match.span()[0], match.span()[0])
    predicate = Predicate(lab, 0, 1)

    return (triple, predicate)


# search predicate in the text with REGULAR EXPRESSIONS
def check_text(label, sentence, triple, start):
    subject_boundary = triple.subject_boundary
    object_boundary = triple.object_boundary

    s=subject_boundary[1]-start

    if(object_boundary==None):               #the literal in the triple don't have boundary->see date_regex_couples
        o=len(sentence)
    else:
        o=object_boundary[0]-start

    subsentence = sentence[s:o]    #search between Subject--Object


    regex = '([^\w](' + label + ')[^\w]|(^' + label + ')|(' + label + '$))'
    pattern = re.compile(regex, re.IGNORECASE)
    # matches = pattern.finditer(subsentence)
    matches = [(0,1)]

    return matches


# delete some labels extracted from Related Words
def limit_labels(labels):
    delete_expression = ["the", "on", "in", "to", "of", "and", "as", "into", "onto"]

    for label in labels:
        if label.lower() in delete_expression:
            labels.remove(label)

    return labels


# substitute in the text with dbo:Class, xsd:date and xsd:double
def modify_text(text_document, triples, start, end, dates_sentence, regex_sentence):
    text = text_document[start:end].strip()
    print("text"+ text)

    entities = []
    for triple in triples:
        subject = text_document[triple.subject_boundary[0]:triple.subject_boundary[1]]
        if triple.object_boundary is not None:
            object = text_document[triple.object_boundary[0]:triple.object_boundary[1]]
        else:
            object = None

        entities.append((triple.subject_ontology, triple.subject_boundary, subject, triple.subject_annotator))
        entities.append((triple.object_ontology, triple.object_boundary, object, triple.object_annotator))

    # get entity without duplicates
    entities_final = []
    for entity in entities:
        if entity is not entities_final:
            entities_final.append(entity)

        # substitute ontologies into the text
        for entity in entities_final:
            if entity[2] is not None:
                ontology = entity[0].replace("http://dbpedia.org/ontology/", "dbo:")
                # text = text[0:entities_to_substitute[index][1][0] - start] + ontology + text[entities_to_substitute[index][1][1] - start:len(text) - start - 1]
                text = re.sub(r'' + entity[2] + '$', ontology, text)
                text = re.sub(r'^' + entity[2] + '', ontology, text)
                text = re.sub(r'[^\w](' + entity[2] + ')[^\w]', " " + ontology + " ", text)

        # check if there are dates and/or literals in the sentence annotated
        (date, literals) = date_and_literals(triples)

        # if there are dates, substitute that dates with "xsd:date"
        if date:
            for date in dates_sentence:
                data = date.surfaceform
                text = re.sub(r'' + data + '$', "xsd:date", text)
                text = re.sub(r'^' + data + '', "xsd:date", text)
                text = re.sub(r'[^\w](' + data + ')[^\w]', " " + "xsd:date" + " ", text)
        # if there are numeric-literals, substitute that dates with "xsd:double"
        if literals:
            for regex in regex_sentence:
                literal = regex.surfaceform
                text = re.sub(r'' + literal + '$', "xsd:double", text)
                text = re.sub(r'^' + literal + '', "xsd:double", text)
                text = re.sub(r'[^\w](' + literal + ')[^\w]', " " + "xsd:double" + " ", text)

    return text


# check if there are date or literals in the triples annotated
def date_and_literals(triples):
    date = False
    regex = False
    for triple in triples:
        if (triple.object_ontology == "http://www.w3.org/2001/XMLSchema%23date"):
            date = True
            break

    for triple in triples:
        if (triple.object_ontology == "http://www.w3.org/2001/XMLSchema%23double"):
            regex = True
            break
    # if date is true = there are dates annotated in the sentence
    # if regex is true = there are literals annotated in the sentence
    return (date, regex)


def limit_triples_and_predicates(predicates, triples):
    triples_cleaned = []
    for triple in triples:
        if not is_in_triples(triple, triples_cleaned):
            triples_cleaned.append(triple)

    predicates_cleaned = []
    for predicate in predicates:
        if not is_in_predicates(predicate, predicates_cleaned):
            predicates_cleaned.append(predicate)

    return (predicates_cleaned, triples_cleaned)


def is_in_triples(triple, triples_cleaned):
    for tri in triples_cleaned:
        if (
                tri.subject_ontology == triple.subject_ontology and tri.predicate_uri == triple.predicate_uri and tri.object_ontology == triple.object_ontology):
            return True

    return False


def is_in_predicates(predicate, predicates_cleaned):
    for pred in predicates_cleaned:
        if (pred.predicate == predicate.predicate):
            return True

    return False


# def modify_entity_with_annotator(document):
#     string = document.text.strip()
#
#     for i in document.entities:
#         y = i.surfaceform
#         annotator_name = i.annotator
#         string = string.strip().replace(y, "<a href=" + i.uri + " target='_blank' class='entity-found'>" + y + "</a>")
#     #   string = re.sub(y, annotator_name + "<a href=" + i.uri + " target='_blank' class='entity-found'>" + y + "</a>",string)
#         document.text = string
#
#     return document
