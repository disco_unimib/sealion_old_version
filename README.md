# SeAlion: a Human-in-the-Loop Approach to Create Training Datasets for RDF Lexicalisation

SeAlion is an open-source tool which allows, via a human-in-the-loop approach, to extract RDF triples from unstructured text and align them to the sentences they represent. SeAlion provides a graphical interface for manually annotating text and navigating the alignments produced automatically by the tool. 

![SeAlion Loaded Abstract section](https://bitbucket.org/disco_unimib/sealion/raw/master/img/abstract.jpg)

![SeAlion Entity Linking section](https://bitbucket.org/disco_unimib/sealion/raw/master/img/el.jpg)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites and installation

A [Docker](https://www.docker.com/) configuration is provided to facilitate installation. This is currently the recommendend and supported way to install SeAlion on your machine. NodeJS/NPM is also required.

The first step is to run 
```
npm install
```
in order to retrieve the required packages. Next, to build and start SeAlion's containers, run

```
docker-compose up
```

SeAlion will be running on localhost port 8096.

## Run
To run the tool execute the following command:
```
docker-compose start
```

