from nltk.tokenize import WordPunctTokenizer
from nltk.tokenize.punkt import PunktSentenceTokenizer, PunktParameters
punkt_param = PunktParameters()
punkt_param.abbrev_types = set(['st', 'dr', 'prof', 'mgr', 'sgt', 'mr', 'mrs', 'inc', 'no', 'etc'])

#PunktParametes è una classe con all'interno dei metodi e dei Set 

class Document:

    def __init__(self, docid, title, pageuri, text, sentence_boundaries=None, words_boundaries=None, entities=None, triples=None):
        """

        initalization of document class
        :param id: wikipedia id of each page  # document id if another text dataset is used.
        :param title: title of the page
        :param pageuri: URI of the item containing the main page
        :param text:  "text that is contained in the page"
        :param sentence_boundaries: start and end offsets of sentences
        :param word_boundaries: list of tuples (start, end) of each word in Wikipedia Article, start/ end are character indices
        :param entities: list of Entities in the document
        :param triples:  list of Triples aligned with sentences in the document
        """

        self.docid = docid
        self.title = title
        self.uri = pageuri
        self.text = text
        self.sentences_boundaries = self.__get_sentences_boundaries() if sentence_boundaries is None else sentence_boundaries
        self.words_boundaries = self.__get_words_boundaries() if words_boundaries is None else words_boundaries
        self.entities = [] if entities is None else entities
        self.triples = [] if triples is None else triples

    @classmethod
    def fromJSON(cls, j):
        """
        instantiate a document class from existing json file
        :param j: j is a json file containing all fields as described in the begining of the document
        """

        docid = j['docid']
        title = j['title']
        uri = j['uri']
        text = j['text']
        sentences_boundaries = j['sentences_boundaries'] if 'sentences_boundaries' in j else None
        word_boundaries =j['words_boundaries'] if 'words_boundaries' in j else None
        entities = [Entity.fromJSON(ej) for ej in j['entities']] if 'entities' in j else None
        triples = [Triple.fromJSON(tj) for tj in j['triples']] if 'triples' in j else None

        return Document(docid, title, uri, text, sentences_boundaries, word_boundaries, entities, triples)
        #ritorna un documento partendo dal JSON

    def __get_sentences_boundaries(self):
        """
        function to tokenize sentences and return
        sentence boundaries of each sentence using a tokenizer.
        :return:
        """

        tokenizer = PunktSentenceTokenizer(punkt_param)        #tokenizer è un oggetto contenente il testo diviso
        sentences = list(tokenizer.span_tokenize(self.text))     #span_tokenize ritorna una tupla con all'interno la 'posizione' di inizio e fine frase
        return sentences

    def __get_words_boundaries(self):
        """
        function to tokenize words in the document and return words
        boundaries of each sentence using a tokenizer.
        :return:
        """
        tokenizer = WordPunctTokenizer()
        words = list(tokenizer.span_tokenize(self.text))
        return words

    def toJSON(self):
        """
        function to print the annotated document into one json file
        :return:
        """
        j = self.__dict__.copy()
        j['entities'] = [i.toJSON() for i in j['entities']] if 'entities' in j and j['entities'] is not None else []
        j['triples'] = [i.toJSON() for i in j['triples']] if 'triples' in j and j['triples'] is not None else []

        return j

    def get_sentences(self):
        """
        :return: get sentences text
        """
        return [self.text[s:e] for s, e in self.sentences_boundaries]

class Entity:
    def __init__(self, uri, boundaries, surfaceform, in_all = False, text = None, img = None ,annotator=None, type_placeholder=None, property_placeholder=None):
        """
        :param uri: entity uri
        :param boundaries: start and end boundaries of the surface form in the sentence
        :param surfaceform: text containing the surface form of the entity
        :param annotator:   annotator used in entity linking
        """
        self.uri = uri
        self.boundaries = boundaries
        self.surfaceform = surfaceform
        self.text = text
        self.img = img
        self.annotator = annotator
        self.type_placeholder = type_placeholder
        self.property_placeholder = property_placeholder
        self.in_all = in_all

    @classmethod
    def fromJSON(cls, j):
        """
        initialize an entity class using a json object
        :param j: json object of an entity
        :return: Entity instantiated object
        """
        annotator = j['annotator'] if 'annotator' in j else None
        type_placeholder = j['type_placeholder'] if 'type_placeholder' in j else None
        property_placeholder = j['property_placeholder'] if 'property_placeholder' in j else None
        return Entity(j['uri'], j['boundaries'], j['surfaceform'], annotator, type_placeholder, property_placeholder)

    def toJSON(self):

        return self.__dict__.copy()

class Triple:

    def __init__(self, subject_uri, subject_boundary, object_uri, object_boundary, subject_ontology, predicate_uri, object_ontology, subject_annotator, object_annotator):
        #subject entity uri
        self.subject_uri = subject_uri
        #subject boundary
        self.subject_boundary = subject_boundary
        #object entity uri
        self.object_uri = object_uri
        #object boundary
        self.object_boundary = object_boundary
        #subject type class (dbo:Class)
        self.subject_ontology = subject_ontology
        #predicate uri (dbo:Class) "http://dbpedia/ontology ..."
        self.predicate_uri = predicate_uri
        #object type class (dbo:Class)
        self.object_ontology = object_ontology
        #subject string
        self.subject = ""
        #predicate string
        self.predicate = ""
        #object string
        self.object = ""
        #subject annotator
        self.subject_annotator = subject_annotator
        #object annotator
        self.object_annotator = object_annotator

    @classmethod
    def fromJSON(cls, j):
        """
        initialize a triple class using a json object
        :param j: json object of an entity
        :return: Triple instantiated object
        """
        print(j)
        # annotator = j['annotator'] if 'annotator' in j else None
        # type_placeholder = j['type_placeholder'] if 'type_placeholder' in j else None
        # property_placeholder = j['property_placeholder'] if 'property_placeholder' in j else None
        # return Triple(j['uri'], j['boundaries'], j['surfaceform'], annotator, type_placeholder, property_placeholder)
        return j

    def toJSON(self):

        return self.__dict__.copy()


class BasePipeline:

    def run(self, document):

        """
        a basic run function for all pipeline components.
        * To Override in every Pipeline components
        :param j: json file containing all annotations per document as defined in the README file
        :return: the same Json after adding annotations.
        """

        return document