import os
import json
import SPARQLWrapper

#query to obtain dbo:Classes in DBpedia rdf:type
def query_type(sub_obj):    
    stringQuery = 'SELECT ?type WHERE {<'+sub_obj+'> a ?type  . FILTER(regex(?type,"http://dbpedia.org/ontology/"))}'
    # stringQuery = 'SELECT ?type WHERE {<'+sub_obj+'> a ?type } LIMIT 3'
    print("string query", stringQuery)
    sparql = SPARQLWrapper.SPARQLWrapper("http://dbpedia.org/sparql")    
    sparql.setQuery(stringQuery)
    sparql.setReturnFormat(SPARQLWrapper.JSON) 
    print("SPARQL",sparql.query().convert())     
    results = sparql.query().convert()      
    if len(results['results']['bindings']) == 0:
        owlThing_uri = {'type':{'type': 'uri', 'value': 'http://www.w3.org/2002/07/owl%23Thing'}}
        results['results']['bindings'].append(owlThing_uri)             
    print("TYPE QUERY RESULTS", results)

    return results

#query to obtain label about a predicate in DBpedia
def query_label(predicate_uri):
    stringQuery = 'select ?label WHERE {<'+predicate_uri+'> rdfs:label ?label .FILTER langMatches( lang(?label), "en" ) }'    
    sparql = SPARQLWrapper.SPARQLWrapper("http://dbpedia.org/sparql")    
    sparql.setQuery(stringQuery)
    sparql.setReturnFormat(SPARQLWrapper.JSON)
    results = sparql.query().convert()    #####oppure .queryAndConvert()

    return results
#parse query of dbo:Classes
def parse_query_response(query_result):

    ontology_list = []
    values = query_result['results']['bindings']
    for uri in values:
        ontology_list.append(uri['type']['value'])

    return ontology_list
#parse abstat query
def parse_abstat_response(response):
    #print(response)
    predicate_list = []
    res = json.loads(response)
    if (len(res['akps']) != 0):
        for predicate in res['akps']:
            predicate_list.append(predicate['predicate']['globalURL'])

    return predicate_list

#parse response to obtain label
def parse_label_response(response):
    if (len(response['results']['bindings']) != 0):
        label = response['results']['bindings'][0]['label']['value']
        return label
    else:
        return ""

#import json (OntologyGraph.json)
def import_json_ontologies():
    with open(os.path.join(os.path.dirname(__file__), '../static/relation_extraction/OntologyGraph.json')) as json_file:
        data = json.load(json_file)

    return data

#import json (english_verbs.json)
def import_json_english_verbs():
    with open(os.path.join(os.path.dirname(__file__), '../static/relation_extraction/english_verbs.json')) as json_file:
        data = json.load(json_file)

    return data