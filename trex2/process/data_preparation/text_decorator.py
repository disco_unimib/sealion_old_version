#Tupla Model to create temporary objects --> (boundary,[(s,e), (s,e)])
class Tupla:
    def __init__(self, boundary):
        self.boundary = boundary
        self.inside_list = []                           


class Decorate_text:

    #create tuple list
    def create_tuple_list(self, lista_tuple):
        tuple_objects = []

        for tupla in lista_tuple:
            t = Tupla(tupla)
            tuple_objects.append(t)

        return tuple_objects
    
    #order tuples created before
    def order(self, tuple_objects):
        ordered_tuple = [tuple_objects[0]]
        tuple_objects = tuple_objects[1:len(tuple_objects)]
        for tupla in tuple_objects:
            i = 0
            pushed = False
            for t in ordered_tuple:
                                                                     ##### tuple_object è una lista di tuple
                if (t.boundary[1] > tupla.boundary[0]):              ##### t è un elemento di ordered_tuple(primo elemento del vecchio tuple_object)
                    if (t.boundary[0] > tupla.boundary[1]):          ##### tupla è un elemento di tuple_object
                        # if they are not nested
                        ordered_tuple.insert(i, tupla)
                        pushed = True
                        break

                    # inside 1
                    elif (t.boundary[0] < tupla.boundary[0] and t.boundary[1] > tupla.boundary[1]):
                        t.inside_list.append(tupla)
                        pushed = True
                        break

                    # inside 2
                    elif (tupla.boundary[0] < t.boundary[0] and tupla.boundary[1] > t.boundary[1]):
                        tupla.inside_list.append(t)
                        ordered_tuple.insert(i, tupla)
                        ordered_tuple.remove(t)
                        pushed = True
                        break

                    # nested 1
                    elif (t.boundary[0] < tupla.boundary[0] and tupla.boundary[0] < t.boundary[1] and
                          tupla.boundary[1] > t.boundary[1]):
                        new_tupla = Tupla((t.boundary[0], tupla.boundary[1]))
                        ordered_tuple.insert(i, new_tupla)
                        ordered_tuple.remove(t)
                        pushed = True
                        break

                    # nested 2
                    elif (t.boundary[0] > tupla.boundary[0] and t.boundary[0] < tupla.boundary[1] and
                          t.boundary[1] > tupla.boundary[1]):
                        new_tupla = Tupla((tupla.boundary[0], t.boundary[1]))
                        ordered_tuple.insert(i, new_tupla)
                        ordered_tuple.remove(t)
                        pushed = True
                        break

                else:
                    i = i + 1

            # add at the end if boundary is greater than all
            if (pushed == False):
                #ordered_tuple.insert(i, tupla)          ## sarebbe uguale se mettessi ordered_tuple.append(tupla)
                ordered_tuple.append(tupla)

        return (ordered_tuple)

    def order_list(self,tuple_objects):

        lista_1 = self.order(tuple_objects)
        lista_2 = self.order(lista_1)

        i = 0
        while(lista_1 != lista_2 and i < 0):
            lista_1 = lista_2
            lista_2 = self.order(lista_1)
            i = i + 1

        return lista_2
    
    #decorate text from the tuple orders executed before
    def apply_decoration(self,text,lista_2):

        new_text = ""

        i = 0
        boundary_index = 0
        boundary_nested_index = 0
        for character in text:

            if (i == lista_2[boundary_index].boundary[0]):
                new_text = new_text + "<span style='background:Aquamarine'>"
                new_text = new_text + text[i]

            elif (i == lista_2[boundary_index].boundary[1]):
                new_text = new_text + "</span>"
                new_text = new_text + text[i]

                if (boundary_index < len(lista_2) - 1):
                    boundary_index = boundary_index + 1

            elif (len(lista_2[boundary_index].inside_list) != 0 and boundary_nested_index < len(lista_2[boundary_index].inside_list) - 1):
                if (i == lista_2[boundary_index].inside_list[boundary_nested_index].boundary[0]):
                    new_text = new_text + "<span style='background:blue'>"
                    new_text = new_text + text[i]

                if (i == lista_2[boundary_index].inside_list[boundary_nested_index].boundary[1]):
                    new_text = new_text + "</span>"
                    new_text = new_text + text[i]

                    if (boundary_nested_index < len(lista_2[boundary_index].inside_list) - 1):
                        boundary_nested_index = boundary_nested_index + 1

            else:
                new_text = new_text + text[i]

            i = i + 1
        return new_text