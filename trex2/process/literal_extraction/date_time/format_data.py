import re
#import datetime

# class that sets the value for a specific pattern
class FormatData:

    def __init__(self):

        # dictionary--> 'season' : 'season_number'
        self.seasons = {
            'summer': 'SU',
            'spring': 'SP',
            'winter': 'WI',
            'autumn': 'FA',
        }

        # dictionary--> 'month' : 'month_number'
        self.months = {
            'january': '01',
            'february': '02',
            'march': '03',
            'april': '04',
            'may': '05',
            'june': '06',
            'july': '07',
            'august': '08',
            'september': '09',
            'october': '10',
            'november': '11',
            'december': '12'
        }

        # dictionary--> 'day' : 'day_number'
        self.days = {
            'monday': '1',
            'tuesday': '2',
            'wednesday': '3',
            'thursday': '4',
            'friday': '5',
            'saturday': '6',
            'sunday': '7'
        }

    # 1) 2000 BC,300 bc, 20 Bc, 8 bC
    def pattern_1(self,stringa):
        data = stringa.split(" ")
        data[0] = int(data[0]) - 1
        date = str(data[0])
        if (len(date) == 3):
            date = '0' + date
        if (len(date) == 2):
            date = '00' + date
        if (len(date) == 1):
            date = '000' + date
        # return date
        return '-'+date

    # 2) 1993.2.12
    def pattern_2(self,stringa):
        data = stringa.split(" ")
        if (len(data) == 3):
            if (len(data[1]) == 1):
                data[1] = '0' + data[1]
            if (len(data[2]) == 1):
                data[2] = '0' + data[2]
            data_string = data[0] + '-' + data[1] + '-' + data[2]
            # return date
            return data_string

    # 3) 11-03-2000
    def pattern_3(self,stringa):
        data = stringa.split(" ")
        if (len(data) == 3):
            if (len(data[0]) == 1):
                data[0] = '0' + data[0]
            if (len(data[1]) == 1):
                data[1] = '0' + data[1]
            data_string = data[2] + '-' + data[1] + '-' + data[0]
            return data_string

    # 4) 1-1-12
    def pattern_4(self,stringa):
        data = stringa.split(" ")
        if (len(data) == 3):
            if (len(data[1]) == 1):
                data[1] = '0' + data[1]
            if (len(data[0]) == 1):
                data[0] = '0' + data[0]
            if (len(data[2]) == 2):
                data[2] = '20' + data[2]
            data_string = data[2] + '-' + data[1] + '-' + data[0]
            return data_string

    # 5) january 8 2000
    def pattern_5(self,stringa):
        data = stringa.split(" ")
        if (len(data[1]) == 1):
            data[1] = '0' + data[1]
        month = self.months[data[0].lower()]
        data_string = data[2] + '-' + month + '-' + data[1]
        return data_string

    # 6) 9 february 2000
    def pattern_6(self,stringa):

        data = stringa.split(" ")
        if (len(data[0]) == 1):
            data[0] = '0' + data[0]
        month = self.months[data[1].lower()]
        data_string = data[2] + '-' + month + '-' + data[0]
        return data_string

    # 7) 2000 february 9
    def pattern_7(self,stringa):
        data = stringa.split(" ")
        if (len(data[2]) == 1):
            data[2] = '0' + data[2]
        month = self.months[data[1].lower()]
        data_string = data[0] + '-' + month + '-' + data[2]
        return data_string

    # 8) 2000 9 february
    def pattern_8(self,stringa):
        data = stringa.split(" ")
        if (len(data[1]) == 1):
            data[1] = '0' + data[1]
        month = self.months[data[2].lower()]
        data_string = data[0] + '-' + month + '-' + data[1]
        return data_string

    # 9) March 1200
    def pattern_9(self,stringa):
        data = stringa.split(" ")
        data[0] = self.months[data[0].lower()]
        date_string = data[1] + '-' + data[0]
        return date_string

    # 10) 1200 march
    def pattern_10(self, stringa):
        data = stringa.split(" ")
        data[1] = self.months[data[1].lower()]
        date_string = data[0] + '-' + data[1]
        return date_string

    # 11) 20th century or 20 century
    def pattern_11(self, stringa):
        data = stringa.split(" ")
        if (('th' in data[0].lower()) or ('st' in data[0].lower())):
            if (len(data[0]) == 4):
                age = int(data[0][0:2]) - 1
                date_string = str(age) + 'XX'
            else:
                age = int(data[0][0:1]) - 1
                date_string = '0' + str(age) + 'XX'

        else:
            age = int(data[0]) - 1
            date_string = str(age) + 'XX'

        return date_string

    # 12) 2000s
    def pattern_12(self, stringa):
        return stringa[0:3] + 'X'

    # 13) summer 2000
    def pattern_13(self, stringa):
        data = stringa.split(" ")
        season = self.seasons[data[0].lower()]
        return data[1]+'-'+season

    # 14) 2000 winter
    def pattern_14(self, stringa):
        data = stringa.split(" ")
        season = self.seasons[data[1].lower()]
        return data[0] + '-' + season

    # 15) summer of 2000
    def pattern_15(self, stringa):
        data = stringa.split(" ")
        season = self.seasons[data[0].lower()]
        return data[2] + '-' + season

    # 16) 2000 of winter
    def pattern_16(self, stringa):
        data = stringa.split(" ")
        season = self.seasons[data[2].lower()]
        return data[0] + '-' + season

    # 17) january of 2000
    def pattern_17(self, stringa):
        data = stringa.split(" ")
        data[0] = self.months[data[0].lower()]
        date_string = data[2] + '-' + data[0]
        return date_string

    # 18) 2000 of january
    def pattern_18(self, stringa):
        data = stringa.split(" ")
        data[2] = self.months[data[2].lower()]
        date_string = data[0] + '-' + data[2]
        return date_string
