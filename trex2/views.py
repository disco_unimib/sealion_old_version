from django.shortcuts import get_object_or_404, render
from django.core.paginator import Paginator

from .models import Abstract, Boundary
from .DB_operations import *

from trex2.process.import_data.abstracts_import import addAbstracts, addSingleAbstract
from trex2.process.data_preparation.data_preparation import DataPreparation

# from trex2.process.data_preparation.coreference.coreference import Coreference_process
# from trex2.process.data_preparation.coreference.stanford_nlp import stanford_nlp
from trex2.process.data_preparation.coreference.coreference_model import Coreference_model
from trex2.process.data_preparation.text_decorator import *

import trex2.process.data_preparation.tasks as dpTasks
import trex2.process.entity_linking.tasks as elTasks
import trex2.process.literal_extraction.tasks as leTasks
import trex2.process.relation_extraction.tasks as reTasks

import trex2.tasks as utilTasks
from trex2.process.entity_linking.entity_model import Document, Entity

from django.http import HttpResponse
from celery.result import AsyncResult
from celery import chain, group
import json
from django.shortcuts import redirect
import re


def index(request):
    string = 'text'
    context = {'text': string}
    return render(request, 'trex2/index.html', context)


def home(request):
    abstract_list = Abstract.objects.order_by('-pub_date')
    paginator = Paginator(abstract_list, 50)

    page = request.GET.get('page')
    abstracts = paginator.get_page(page)

    context = {'abstract_count': abstract_list.count, 'abstract_list': abstracts}
    return render(request, 'trex2/home.html', context)


def abstract_show(request, abstract_id):
    abstract = get_object_or_404(Abstract, id=abstract_id)
    context = {'abstract': abstract}
    return render(request, 'trex2/abstract/show.html', context)


def abstract_new(request):
    string = 'text'
    context = {'text': string}
    return render(request, 'trex2/abstract/new.html', context)


def abstract_import(request):
    string = 'text'
    context = {'text': string}
    return render(request, 'trex2/abstract/import.html', context)


def abstract_delete_all(request):
    job = utilTasks.delete_all_abstracts.delay()

    context = {'title': 'Deleting all abstracts', 'task_id': job.id}
    return render(request, 'trex2/generic_loader.html', context)


def delete_all_results(request):
    all_abstracts = Abstract.objects.all()

    for abstract in all_abstracts:
        abstract.is_done_dp = False
        abstract.is_done_el = False
        abstract.is_done_le = False
        abstract.is_done_re = False
        abstract.percentage = 0
        abstract.save()

    return redirect('home')


def delete_abstract(request, abstract_id):
    abstract = get_object_or_404(Abstract, id=abstract_id)

    abstract.delete()

    return redirect('home')


def delete_results(request, abstract_id):
    abstract = get_object_or_404(Abstract, id=abstract_id)

    abstract.is_done_dp = False
    abstract.is_done_el = False
    abstract.is_done_le = False
    abstract.is_done_re = False
    abstract.percentage = 0
    abstract.save()

    return redirect('home')


def starting_abstract(request, abstract_id):
    abstract = get_object_or_404(Abstract, id=abstract_id)

    context = {'abstract': abstract}
    return render(request, 'trex2/process/starting.html', context)


# ---------------------------------------- Data Preparation --------------------------------------------------
def data_preparation(request, abstract_id):
    (boundaries_list, abstract) = dpTasks.data_preparation_process(abstract_id)

    # ---------------------------------------------------------
    # decorate text to visualize removed text
    # text decoration
    if (len(boundaries_list) != 0):
        decorator_text = Decorate_text()
        # create list of tuple --> [(b1,b2),(b1,b2)] --> (b1,b2) is a Boundary
        tuple_objects = decorator_text.create_tuple_list(boundaries_list)

        # apply decoration to words represented by Boundaries
        new_text = decorator_text.apply_decoration(abstract.text, decorator_text.order_list(tuple_objects))
    else:
        new_text = abstract.text

    print(boundaries_list)
    context = {'abstract': abstract, 'new_text': new_text}
    return render(request, 'trex2/process/0-data-preparation.html', context)


# ---------------------------------------- Execute all abstracts --------------------------------------------------
def data_preparation_all(request):
    all_abstracts = Abstract.objects.all()
    abstracts_jobs = []
    for abstract in all_abstracts:
        abstract_job = chain(
            dpTasks.data_preparation_process.si(abstract.id, True),
            elTasks.entity_linking_process.si(abstract.id, True),
            leTasks.literal_extraction_process.si(abstract.id, True),
            reTasks.relation_extraction_process.si(abstract.id, True)
        )
        abstracts_jobs.append(abstract_job)

    parallel_job = group(abstracts_jobs)
    parallel_job.apply_async()

    return redirect('home')


# ---------------------------------------- Entity Linking --------------------------------------------------
def entity_linking(request, abstract_id):
    # get document and abstract from entity linking process method
    (document, abstract) = elTasks.entity_linking_process(abstract_id)

    # color text linked entities
    # create text with highlight entities
    string = document.text.strip()
    string_substituted = []
    for i in document.entities:
        y = i.surfaceform
        annotator_name = i.annotator
        if y.lower() not in string_substituted:
            # string = string.strip().replace(y,"<a href=" + i.uri + " target='_blank' class='entity-found'>" + y + "</a>")
            string = re.sub(y, "<a href=" + i.uri + " target='_blank' class='entity-found'>" + y + "</a>", string)
            string_substituted.append(y.lower())
    # end color linked entities
    # abstract = abstract, document = Document object, text = decorated text
    context = {'abstract': abstract, 'document': document, 'text': string}
    return render(request, 'trex2/process/1-entity-linking.html', context)


# ---------------------------------------- Literal Extraction --------------------------------------------------
def literal_extraction(request, abstract_id):
    # get document, abstract and type from literal extraction process method
    (abstract, document, type) = leTasks.literal_extraction_process(abstract_id)

    string = document.text.strip()

    # abstract = abstract, document = Document object, type = list of Date and Regex
    context = {'abstract': abstract, 'document': document, 'text': string, 'type': type}
    return render(request, 'trex2/process/2-literal-extraction.html', context)


# ---------------------------------------- Relation Extraction --------------------------------------------------
def relation_extraction(request, abstract_id):
    # get abstract, abstract_text and sentences from relation extraction process method
    (abstract, abstract_text, sentences) = reTasks.relation_extraction_process(abstract_id)

    # abstract_text= modify_text_with_annotator(abstract_text)
    ####[Matt]
    # entity_list = get_el(abstract)
    # string_substituted = []

    # for i in entity_list:
    #    y = i.surfaceform
    #    annotator_name=i.annotator
    #    if y.lower() not in string_substituted:
    #    #string = string.strip().replace(y,"<a href=" + i.uri + " target='_blank' class='entity-found'>" + y + "</a>")
    #        abstract_text = re.sub(y, "<a href="+i.uri+" target='_blank' class='entity-found'>"+y+"</a>",abstract_text)
    #        string_substituted.append(y.lower())

    # for sentence in sentences:
    #    #sentence.sentence=sentence.sentence.replace("Wikifier","<span class=\"annotator wikifier\"></span>")
    #    #sentence.sentence=sentence.sentence.replace("Wikidata_Spotlight_Entity_Linker","<span class=\"annotator wikidata_spotlight_entity_linker\"></span>")
    #    sentence.sentence=sentence.sentence.replace("Wikifier","")
    #    sentence.sentence=sentence.sentence.replace("Wikidata_Spotlight_Entity_Linker","")
    ####

    # abstract = abstract, abstract_text = text, sentences = list of sentences of the text and the triples extracted
    context = {'abstract': abstract, 'text': abstract_text, 'sentences': sentences}
    return render(request, 'trex2/process/3-relation-extraction.html', context)


def annotation(request, abstract_id):
    abstract = get_object_or_404(Abstract, id=abstract_id)

    sentences = ["Elliott Smith was an American singer-songwriter and multi-instrumentalist.",
                 "Elliott Smith also played piano, clarinet, bass guitar, drums and harmonica."]

    context = {'abstract': abstract, "sentences":sentences}
    # string = 'text'
    # context = {'text': string}
    return render(request, 'trex2/process/4-annotation.html', context)


def process_data(request):
    if (request.POST['testo'] is not None):
        # add single AbstractData
        addSingleAbstract(request.POST['testo'])

        return redirect('home')


def process_data_all(request):
    # add all AbstractData
    job = addAbstracts.delay()

    context = {'title': 'Importing', 'task_id': job.id}
    return render(request, 'trex2/generic_loader.html', context)



def get_progress(request, task_id):
    result = AsyncResult(task_id)
    response_data = {
        'state': result.state,
        'details': result.info,
    }
    return HttpResponse(json.dumps(response_data), content_type='application/json')
