# coding=utf-8
import os
import app.settings
import json
from trex2.process.data_preparation.coreference.coreference_model import Coreference_model
from stanfordcorenlp import StanfordCoreNLP

#stanford coreference class
class stanford_nlp:

    def __init__(self,document):
        self.document = document
        self.coref_list = []

    def get_boundaries(self,mentions, sentences):
        #save subject to substitute
        subj = mentions[0]
        for j in mentions:
            if (j['text'].lower() != subj['text'].lower() and j['type'] == 'PRONOMINAL'):
                boundary = self.get_boundary(j, sentences)
                coref_instance = Coreference_model(j['text'],subj['text'],boundary)
                self.coref_list.append(coref_instance)

    def get_boundary(self,single_word, sentences):
        #get sentence that contains the word (single_word)
        sentence = sentences[single_word['sentNum'] - 1]
        word_start = sentence['tokens'][single_word['startIndex'] - 1]
        word_end = sentence['tokens'][single_word['endIndex'] - 2]
        #get boundaries of the pronoun
        boundary = (word_start['characterOffsetBegin'], word_end['characterOffsetEnd'])
        #return boundary
        return boundary

    def stanford_process(self):
        #create connection with stanford_nlp service
        nlp = StanfordCoreNLP('http://stanford_nlp',9000)
        #create property dictionary for stanford_nlp (coreference)
        pros = {'annotators': 'coref', 'pinelineLanguage': 'en'}
        #get json response from stanford_nlp process
        result_dict = json.loads(nlp.annotate(self.document.text, properties=pros))
        #close connection
        nlp.close()
        print("StanfordCoreNLP")
        #get coreference from json
        coreference = result_dict['corefs']
        # get sentences analization from json
        sentences = result_dict['sentences']
        #get_boundaries for every coreference founded in the text
        for idx, mentions in coreference.items():
            self.get_boundaries(mentions, sentences)

        return self.coref_list
