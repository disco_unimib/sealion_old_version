from celery import shared_task
from django.shortcuts import get_object_or_404
from celery import task, current_task
from trex2.models import Abstract
from trex2.DB_operations import save_re, get_el, get_le
from trex2.process.entity_linking.entity_model import Document
from trex2.process.relation_extraction.predicate_extraction import *

#--------------relation extraction process------------------
@shared_task
def relation_extraction_process(abstract_id, is_process_all = False):
    abstract = get_object_or_404(Abstract, id=abstract_id)
    print("RELATION EXTRACTION OF", abstract.concept, "--------", abstract.id)
    if abstract.is_done_re and is_process_all:
        return
    
    abstract_text = abstract.lexprocess.dp.output
    if(abstract.is_done_re):

        #get sentences from DB
        sentences = abstract.lexprocess.re.sentences
        if (is_process_all == False):
            return (abstract, abstract_text, sentences)
    
    #if entity linking and literal extraction were done, relation extraction can be executed
    if (abstract.is_done_el and abstract.is_done_le):

        abstractToLink = abstract.lexprocess.dp
        #get entity linking results
        entity_list = get_el(abstract)
        document = Document(abstract.uri, abstract.concept, abstract.uri, abstractToLink.output, entities=entity_list)
        #get literal extraction results
        (document_date, regex) = get_le(abstract)
        #dates = list of dates
        dates = document_date.entities

        #calculate predicates and triples
        sentences = predicate_annotation(document, dates, regex)
        #save predicates and triples
        save_re(abstract, sentences)

        if (is_process_all == False):
            return (abstract, abstract_text, sentences)
    else:
        if (is_process_all == False):
            return (abstract, abstract_text, [])