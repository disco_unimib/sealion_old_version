# -*- coding: utf-8 -*-

import json
import requests
from .entity_model import Entity,BasePipeline


# Dandelion entity linker
"""
        :param lang: Lang of the document to process
        :param threshold: threshold used to find Entities
        :param annotator: name of annotator (Dandelion)
        """
class DandelionEntityLinker:

    #init method for Dandelion
    def __init__(self,lang="en", threshold=0.6):
        self.lang=lang
        self.threshold=threshold
        self.annotator = "Dandelion"

    #HTTP POST request method to Dandelion
    def Http_Post_request(self,url,values):
        # Http post to Dandelion
        r = requests.get(url, params=values)
        # end of HTTP POST request
        return r.text

    def run(self,document):

        try:
            #set url and values for request to Dandelion
            url = 'https://api.dandelion.eu/datatxt/nex/v1/'
            values = {'token': '68d8cb6f94864ec48c603652ac96d918',
                            'text': document.text,
                            'lang': self.lang,
                            'min_confidence': self.threshold,
                            'include': 'types'}

            response = self.Http_Post_request(url,values)

            #Json parse for Entity
            json_converter = json.loads(response)
            # take from JSON: uri,(start,end), title
            for i in json_converter["annotations"]: #Take annotations list of object i json
                boundaries = i["spot"] # take the first word matching in the setence
                all_boundaries = []
                for j in boundaries:
                    start_end = (j["start"], j["end"] + 1)
                    all_boundaries.append(start_end)
                entity = Entity(
                    uri = i['lod']['dbpedia'], #uri dbpedia
                    boundaries = all_boundaries, #[(start,end),(start,end)]
                    surfaceform = i["types"], #concept
                    annotator=self.annotator) #annotator name

                #Add Etity to document.entities list
                document.entities.append(entity)

        except Exception as e:
            print ("")

        #return document
        return document