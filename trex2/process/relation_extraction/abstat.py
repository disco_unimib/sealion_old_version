import requests
import json

#abstat call to obtain predicate from couples like (subject, object) or (subject, literal), CHANGE LIMIT PARAMETER TO OBTAIN MORE PREDICATES
def abstat_call(subject, object):
    unused_preds = ["http://dbpedia.org/ontology/wikiPageWikiLink","http://www.w3.org/2002/07/owl#sameAs","http://dbpedia.org/property/wikiPageUsesTemplate","http://dbpedia.org/ontology/wikiPageExternalLink","http://dbpedia.org/ontology/selection","http://dbpedia.org/property/selection","http://www.w3.org/ns/prov#wasDerivedFrom","http://dbpedia.org/property/hasPhotoCollection","http://xmlns.com/foaf/0.1/depiction","http://dbpedia.org/ontology/thumbnail","http://dbpedia.org/ontology/type","http://purl.org/linguistics/gold/hypernym","http://xmlns.com/foaf/0.1/homepage"]
    if object == 'http://www.w3.org/2002/07/owl%23Thing':
        url = 'http://abstat.disco.unimib.it/api/v1/browse?enrichWithSPO=true&subj=' + subject + '&obj=' + object + '' 
        r = requests.get(url)        
    else:
        url = 'http://abstat.disco.unimib.it/api/v1/browse?enrichWithSPO=true&limit=5&subj=' + subject + '&obj=' + object + ''        
        r = requests.get(url)    

    akps = json.loads(r.text)        
    clean_akps = [predicate for predicate in akps["akps"] if predicate["predicate"]["globalURL"] not in unused_preds]  
    cleaned_akps = {"akps":clean_akps[0:4]}
    print("ABSTAT res", json.dumps(cleaned_akps))
    return (json.dumps(cleaned_akps))               ###### r.text contiene un testo con all'interno una "lista" di json


#related words api to obtain predicate's related words
def call_related_words_api(word):
    url = 'https://relatedwords.org/api/related?term=' + word + ''
    r = requests.get(url)

    #parse json response and keep only words with score > 1
    json_load = json.loads(r.text)
    labels = []
    for label in json_load:
        if label['score'] is not None:
            if label['score'] > 1:
                labels.append(label['word'])
    
    print("SYNS", labels)
    return labels


#oxford api (not used because the response is strange)
def call_oxford_api(word):
    app_id = "7b3b2f6c"
    app_key = "f3968288bc4c4aca7a34277c6aa26359"
    language = "en-gb"
    url = "https://od-api.oxforddictionaries.com:443/api/v2/entries/" + language + "/" + word.lower()
    print("url : " + url)
    r = requests.get(url, headers={"app_id": app_id, "app_key": app_key})
    json_load = json.loads(r.text)

