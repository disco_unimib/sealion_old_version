from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('home/', views.home, name='home'),
    # abstract
    path('abstract/<int:abstract_id>/show/', views.abstract_show, name='abstract_show'),
    path('abstract/new/', views.abstract_new, name='abstract_new'),
    path('abstract/import/', views.abstract_import, name='abstract_import'),
    path('abstract/process/', views.process_data, name='process_data'),
    path('abstract/process_all/', views.process_data_all, name='process_data_all'),
    path('abstract/delete-all/', views.abstract_delete_all, name='delete_all'),
    path('abstract/delete-all-results/', views.delete_all_results, name='delete_all_results'),
      # annotation
    # path('annotation/', views.annotation, name='annotation'),
    # process
    path('process/starting-abstract/<int:abstract_id>/', views.starting_abstract, name='starting_abstract'),
    path('process/data-preparation/<int:abstract_id>/', views.data_preparation, name='data_preparation'),
    path('process/data-preparation-all/', views.data_preparation_all, name='data_preparation_all'),
    path('process/entity-linking/<int:abstract_id>/', views.entity_linking, name='entity_linking'),
    path('process/literal-extraction/<int:abstract_id>/', views.literal_extraction, name='literal_extraction'),
    path('process/relation_extraction/<int:abstract_id>/', views.relation_extraction, name='relation_extraction'),
    path('process/annotation/<int:abstract_id>/', views.annotation, name='annotation'),

    path('process/delete_abstract/<int:abstract_id>/', views.delete_abstract, name='delete_abstract'),
    path('process/delete-results/<int:abstract_id>/', views.delete_results, name='delete_results'),

    # utils
    path(r'(<task_id>[\w-]+)/', views.get_progress, name='get_progress'),
]
