from celery import shared_task
from django.shortcuts import get_object_or_404
from celery import task, current_task
from trex2.models import Abstract
from trex2.DB_operations import get_el, save_el
import trex2.process.data_preparation.tasks as dpTasks
from trex2.process.entity_linking.entity_model import Document, Entity
#from trex2.process.entity_linking.spotlight import WikidataSpotlightEntityLinker
from trex2.process.entity_linking.dandelion import DandelionEntityLinker
from trex2.process.entity_linking.wikifier import WikifierEntityLinker
from trex2.process.entity_linking.fusion import FusionUnion , FusionIntersection

#--------------entity linking process------------------
@shared_task
def entity_linking_process(abstract_id, is_process_all = False):
    abstract = get_object_or_404(Abstract, id=abstract_id)
    print("ENTITY LINKING OF", abstract.concept, "--------", abstract.id)
    if abstract.is_done_el and is_process_all:
        return
        

    #if abstract was just processed or not
    if (abstract.is_done_el == False):

        if abstract.is_done_dp == False:
            dpTasks.data_preparation_process(abstract.id, True)

        abstractToLink = abstract.lexprocess.dp
        document = Document(abstract.uri, abstract.concept, abstract.uri, abstractToLink.output)
        # create wikifier object
        wikifier = WikifierEntityLinker()
        # create spotlight object
        #spotlight = WikidataSpotlightEntityLinker()
        # Entity linking.....using the union between spotlight's annotated entities and wikifier's annotated entities
        #fusionUnion = FusionUnion([wikifier, spotlight], document)
        #calculate the entity linking
        #document = fusionUnion.annotate(document)
        dandelion = DandelionEntityLinker()
        fusionIntersect = FusionIntersection([wikifier, dandelion], document)
        document = fusionIntersect.annotate(document)

        #save entity linking results
        save_el(abstract, document)
    
    else:
        
        abstractToLink = abstract.lexprocess.dp
        #get result of entity linking that was in DB
        entity_list = get_el(abstract)
        #create Document object
        document = Document(abstract.uri, abstract.concept, abstract.uri, abstractToLink.output, entities=entity_list)
    if (is_process_all == False):
        return (document, abstract)
