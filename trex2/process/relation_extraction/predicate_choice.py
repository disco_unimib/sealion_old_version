from nltk.corpus import wordnet
import nltk
from nltk import word_tokenize


#obtain a synonyms of the word
def wordnet_synonyms(word):
    synonyms = []
    for syn in wordnet.synsets(word):
        for l in syn.lemmas():
            synonyms.append(l.name())

    return synonyms

#check the similarity between the word and each word in a sentence
def wordnet_similarity(word, sentence):
    similarity_word = []
    sentence = sentence.replace('.','')
    sentence = sentence.replace(',','')
    word_list = sentence.split(" ")

    for word in word.split(" "):
        try:
            w1 = wordnet.synset( word.strip() + '.n.01')
        except : #WordNetError ---> No exist a lemma of the word
            return similarity_word

        for word2 in word_list:
            try:
                w2 = wordnet.synset( word2.strip() +'.n.01')
                if(w1.wup_similarity(w2) >= 0.60):
                    similarity_word.append(word2)
            except :   #WordNetError ---> No exist a lemma of the word
                continue

    return similarity_word

# def get_verb_from_sentence(sentence):
#     list_of_word = nltk.pos_tag(sentence)
#     list_of_verb = []
#
#     for word in list_of_word:
#         if 'VB' in word[1]:
#             list_of_verb.append(word)
#
#     return(list_of_verb)


def get_verb_from_sentence(sentence):
    text = word_tokenize(sentence)
    list_of_word = nltk.pos_tag(text)
    list_of_verb = []

    for index, word in enumerate(list_of_word):
        if 'VB' in word[1]:
            if 'VB' in word[1] and 'VBN' not in list_of_word[index + 1][1]:
                list_of_verb.append(word[0])

    return (list_of_verb)


def nounify(verb_word):

    set_of_related_nouns = set()
    try:             # esistono dei verb in cui wordnet.morphy non ritorna Nulla
        for lemma in wordnet.lemmas(wordnet.morphy(verb_word, wordnet.VERB), pos="v"):
            for related_form in lemma.derivationally_related_forms():
                for synset in wordnet.synsets(related_form.name(), pos=wordnet.NOUN):
                    set_of_related_nouns.add(synset)
    except:
        return set_of_related_nouns

    return set_of_related_nouns

def similarity_between_verb_predicate(word, sentence):

    verbs = get_verb_from_sentence(sentence)
    verb_to_noun_list = []

    for verb in verbs:
        for i in nounify(verb):
            if ('n.01' in str(i.name)):
                verb_to_noun_list.append(i)

    word_synset = wordnet.synsets(word, pos='n')

    if len(word_synset)>0 and len(verb_to_noun_list)>0:
        for verb in verb_to_noun_list:
            for word in word_synset:
                if (word.wup_similarity(verb) >= 0.50):
                    #return word
                    return verbs
        return [] 
    else:
        return [] 


def find_terms_verb_predicate(list_of_word, sentence):

    verbs = get_verb_from_sentence(sentence)
    verb_to_noun_list = []
    list_of_synset = []
    word_synset = []

    for verb in verbs:
        for i in nounify(verb):
            if ('n.01' in str(i.name)):
                verb_to_noun_list.append(i)

    for i in list_of_word:
        word_synset = wordnet.synsets(i, pos='n')
        list_of_synset.append(word_synset)

    word_list=[]
    final_list=[]

    if len(word_synset)>0 and len(verb_to_noun_list)>0:
        for verb in verb_to_noun_list:
            for word in word_synset:
                if (word.wup_similarity(verb) >= 0.50):
                    word_list.append(word)

    for synset in word_list:
        for lemma in synset.lemmas():
            final_list.append(lemma.name())

    return(final_list)




