# -*- coding: utf-8 -*-

from .entity_model import Entity,BasePipeline
import spotlight

class WikidataSpotlightEntityLinker(BasePipeline):

    def __init__(self, spotlight_url='https://api.dbpedia-spotlight.org/en/annotate', confidence=0.5, support=1):
        """
        :param db_wd_mapping: csv file name containing mappings between DBpedia URIS and Wikdiata URIS
        :param spotlight_url: url of the dbpedia spotlight service
        :param confidence: min confidence
        :param support:  min supporting document
        """

        self.annotator_name = 'Wikidata_Spotlight_Entity_Linker'
        self.spotlight_url = spotlight_url
        self.confidence = confidence
        self.support = support

    def run(self, document):

        """
        :param document: Document object
        :return: Document after being annotated
        """

        #document.entities = []

        for sid, (start, end) in enumerate(document.sentences_boundaries):

            try:
                #request to spotlight
                annotations = spotlight.annotate(self.spotlight_url,
                                                 document.text[start:end], #document.text[start:end],
                                                 self.confidence,
                                                 self.support)

            except Exception as e:
                annotations = []

            #parse json given from spotlight
            for ann in annotations:

                e_start = document.sentences_boundaries[sid][0] + ann['offset']    ## il campo offset contiene il punto di inizio
                                                                                   ## dell'elemento all'interno della frase

                if type(ann['surfaceForm']) not in [str, str]:
                    ann['surfaceForm'] = str(ann['surfaceForm'])

                e_end = e_start + len(ann['surfaceForm'])                           ## il campo surfaceForm contiene il nome dell'elemento annotato

                #create Entity object with all properties
                entity = Entity(uri=ann['URI'],
                                boundaries=[(e_start, e_end)],
                                surfaceform=ann['surfaceForm'],
                                annotator=self.annotator_name)

                document.entities.append(entity)

        #for ent in document.entities:
            #print(f"spot:{ent.uri}")

        return document
